from django import forms
from django.forms import widgets
from django.forms.models import fields_for_model
from .models import Reservadehora, Usuarioescritorio

class ReservadeHoraForm(forms.ModelForm):
    asunto = forms.CharField(widget=forms.Textarea(attrs={'rows':4}))
    hora = forms.CharField()
    class Meta:
        model = Reservadehora
        fields = ('fecha_solicitud','asunto','tiposervicio','usuarioescritorio')
    
    def __init__(self, *args, **kwargs):

        super(ReservadeHoraForm, self).__init__(*args,**kwargs)
        self.fields['usuarioescritorio'] = forms.ModelChoiceField(queryset=Usuarioescritorio.objects.filter(tipousuario_id=1))

        for field in self.fields:
            
            self.fields[field].widget.attrs.update({'class': 'form-control'})
            self.fields[field].widget.attrs.update({'autocomplete': 'off'})

        self.fields['hora'].widget.attrs.update({'class': 'form-control timepicker'})        
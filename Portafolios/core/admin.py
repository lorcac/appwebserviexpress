from django.contrib import admin
from .models import UsuarioWeb,Boleta,Reservadehora,Tiposervicio,Tipousuario,Usuarioescritorio


admin.site.register(UsuarioWeb)
admin.site.register(Boleta)
admin.site.register(Reservadehora)
admin.site.register(Tiposervicio)
admin.site.register(Tipousuario)
admin.site.register(Usuarioescritorio)
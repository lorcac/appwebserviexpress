from django.urls import path
from . import views

app_name ='core'

urlpatterns = [
   path('', views.index, name='index'),
   path('perfil/', views.perfil, name='perfil'),
   path('reservar_hora/', views.reserva_de_hora, name='reserva_de_hora'),

]

from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .forms import ReservadeHoraForm
from django.db import connection
from .models import Reservadehora

def index(request):

    return render(request,'core/index.html',{})

@login_required(login_url='/usuarios/login/')
def reserva_de_hora(request):

    form_reserva = ReservadeHoraForm()

    if request.method == 'POST':
        
        form_reserva = ReservadeHoraForm(data=request.POST)

        if form_reserva.is_valid():

            fecha_solicitud = form_reserva.data['fecha_solicitud']
            hora = form_reserva.data['hora']
            asunto = form_reserva.data['asunto']
            tiposervicio = form_reserva.data['tiposervicio']
            usuarioescritorio = form_reserva.data['usuarioescritorio']
            usuarioweb = request.user.id
            print(request.user.id)
            print(fecha_solicitud)
            print(hora)
            fecha = fecha_solicitud + ' ' + hora
            print(fecha)


            with connection.cursor() as cursor:
                cursor.callproc('PKG_RESERVADEHORA.PR_AGREGAR_RESERVADEHORA',[fecha,asunto,tiposervicio,usuarioescritorio,usuarioweb])
            
            return redirect('core:index')


    return render(request,'core/reserva_de_hora.html',{'form':form_reserva})

@login_required(login_url='/usuarios/login/')
def perfil(request):

    return render(request,'core/perfil.html',{})